#!/bin/bash

DetermineArchitecture()
{
    ret=0
    case `uname -m` in
    x86_64)
        echo "x86_64";;
    i?86)
        echo "i386";;
    *)
        echo "Unsupported machine architecture."
        ret=1;;
    esac
    return ${ret}
}

DetermineOperatingSystem()
{
    ret=0
    case `uname -s` in
    Linux)
        echo "linux-gnu";;
    *)
        echo "Unsupported operating system."
        ret=1;;
    esac
    return ${ret}
}

arch=`DetermineArchitecture`
ret=$?
echo ${arch}
if [ ${ret} -ne 0 ]
then
    exit ${ret}
fi

os=`DetermineOperatingSystem`
ret=$?
echo ${os}
if [ ${ret} -ne 0 ]
then
    exit ${ret}
fi

if [ ${CC} ]
then
        export CC="$CC -ggdb"
else
        export CC="gcc -ggdb"
fi

if [ ${CXX} ]
then
        export CXX="$CXX -ggdb"
else
        export CXX="g++ -ggdb"
fi

mkdir build_${arch}-${os} &&
cd build_${arch}-${os} &&
cmake ../source \
-DCMAKE_INSTALL_PREFIX=${PWD}/../${arch}-${os} \
-DWAVE=FALSE \
-DALSOFT_CONFIG=FALSE \
-DEXAMPLES=FALSE \
-DREQUIRE_ALSA=TRUE \
-DREQUIRE_OSS=TRUE \
-DREQUIRE_PULSEAUDIO=TRUE \
-DUTILS=FALSE &&
make &&
make install &&
cp ${PWD}/../source/COPYING ${PWD}/../${arch}-${os}/lib/LICENSE-openal-soft.txt &&
cd .. &&
rm -rf build_${arch}-${os} &&
rm -rf ${arch}-${os}/lib/pkgconfig && # we do not use pkg-config
echo "Build for ${arch}-${os} is ready."