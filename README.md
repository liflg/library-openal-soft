Website
=======
http://kcat.strangesoft.net/openal.html

License
=======
LGPL (see the file source/COPYING)

Version
=======
1.15.1

Source
======
openal-soft-1.15.1.tar.bz2 (sha256: 0e29a162f0841ccb4135ce76e92e8a704589b680a85eddf76f898de5236eb056)